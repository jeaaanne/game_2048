/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_game.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 12:34:27 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 17:09:55 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

t_game		*init_game(t_game *game)
{
	game = (t_game *)malloc(sizeof(*game) * 1);
	game->won = 0;
	game->lost = 0;
	game->tiles = init_tiles();
	game->init = 0;
	game->moved = 0;
	return (game);
}
