/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_tiles.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 14:48:56 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 17:06:09 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

static void		set_rotate(t_game *game, int direction)
{
	if (direction == KEY_UP)
		game->rotate_nb = 3;
	if (direction == KEY_DOWN)
		game->rotate_nb = 1;
	if (direction == KEY_RIGHT)
		game->rotate_nb = 2;
	if (direction == KEY_LEFT)
		game->rotate_nb = 0;
}

static void		push_tiles(t_game *game)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (x < 4)
	{
		y = 1;
		while (y < 4)
		{
			if (game->tiles[x][y] != 0 && game->tiles[x][y - 1] == 0)
			{
				game->tiles[x][y - 1] = game->tiles[x][y];
				game->tiles[x][y] = 0;
				game->moved += 1;
			}
			y++;
		}
		x++;
	}
}

static void		rotate(t_game *game)
{
	while ((game->rotate_nb)--)
		game = rotate_tab(game);
	push_tiles(game);
	push_tiles(game);
	push_tiles(game);
	push_tiles(game);
}

static void		merge_cell(t_game *game, int x, int y)
{
	if (game->tiles[x][y] == game->tiles[x][y - 1] && \
			game->tiles[x][y] != 0)
	{
		game->tiles[x][y - 1] *= 2;
		game->tiles[x][y] = 0;
		push_tiles(game);
		game->moved += 1;
	}
}

int				merge_tiles(t_game *game, int direction)
{
	int x;
	int y;

	x = 0;
	y = 0;
	game->moved = 0;
	set_rotate(game, direction);
	rotate(game);
	while (x < 4)
	{
		y = 1;
		while (y < 4 && y - 1 < 4)
		{
			merge_cell(game, x, y);
			y++;
		}
		x++;
	}
	set_rotate(game, direction);
	game->rotate_nb = 4 - game->rotate_nb;
	while ((game->rotate_nb)--)
		game = rotate_tab(game);
	return (game->moved);
}
