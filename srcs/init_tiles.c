/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tiles.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:09:36 by jbailhac          #+#    #+#             */
/*   Updated: 2015/03/01 17:45:33 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_rush.h"

int			**init_tiles(void)
{
	int		**tiles;
	int		i;
	int		j;

	i = 0;
	tiles = (int **)malloc(sizeof(*tiles) * 5);
	if (!tiles)
		return (NULL);
	while (i <= 4)
	{
		tiles[i] = (int *)malloc(sizeof(**tiles) * 5);
		if (!tiles[i])
			return (NULL);
		j = 0;
		while (j <= 4)
		{
			tiles[i][j] = 0;
			j++;
		}
		i++;
	}
	return (tiles);
}
