# 2048 #

A simple C implementation of the game [2048](http://gabrielecirulli.github.io/2048/) for the shell.

### set up ###
```
git clone https://jeaaanne@bitbucket.org/jeaaanne/game_2048.git
cd game_2048
make
./game_2048
```
![2048_image](http://i.imgur.com/XwtNcYB.png?1)