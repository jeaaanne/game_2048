/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 12:14:27 by jbailhac          #+#    #+#             */
/*   Updated: 2014/11/08 19:03:32 by jbailhac         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_striter(char *s, void (*f)(char *))
{
	char *tmp;

	if (s == NULL || f == NULL || !(*s))
		return ;
	tmp = s;
	while (*tmp != '\0' && tmp != NULL && f != NULL)
	{
		f(tmp);
		tmp++;
	}
}
