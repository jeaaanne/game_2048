# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/03 11:46:23 by jbailhac          #+#    #+#              #
#    Updated: 2015/03/01 18:09:13 by jbailhac         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC			=gcc

CFLAGS		=-Wall -Werror -Wextra

NAME		=game_2048

SRC_PATH	=./srcs/

SRC_NAME	=main.c			\
			init_game.c		\
			init_tiles.c	\
			print_game.c	\
			merge_tiles.c	\
			menu.c			\
			win.c			\
			rotate.c		\
			update_game.c	\
			lost.c			\
			is_full.c

OBJ_PATH	=./objs/

OBJ_NAME	=$(SRC_NAME:.c=.o)

SRC			=$(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ			=$(addprefix $(OBJ_PATH),$(OBJ_NAME))

.PHONY:		all clean fclean re

all:		$(NAME)

clean:
			@/bin/rm -f $(OBJ)
			@rmdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@echo "\x1b[32mobjects cleaned.\x1b[0m"

fclean:		clean
			@/bin/rm -f $(NAME)
			@echo "\x1b[32m$(NAME) cleaned.\x1b[0m"

$(NAME):	$(OBJ)
			@make -C libft/ >/dev/null
			@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) libft/libft.a -lncurses -lmenu
			@echo "\x1b[32m$(NAME) done.\x1b[0m"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
			@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@$(CC) -c $(CFLAGS) -o $@ $^
			@echo "$< compiled"

re:			fclean all
